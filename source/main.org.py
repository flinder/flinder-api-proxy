# [START app]
import logging

from flask import Flask, jsonify

# [START imports]
import requests
import requests_toolbelt.adapters.appengine
import json
from bs4 import BeautifulSoup
import re
import json



my_list = []
mydict = {}
# Use the App Engine Requests adapter. This makes sure that Requests uses
# URLFetch.
requests_toolbelt.adapters.appengine.monkeypatch()
# [END imports]

app = Flask(__name__)

@app.route('/buses/')
def index():
    # [END requests_get]
    my_list = []
    mydict = {}
    url = 'http://api.sl.se/api2/realtimedeparturesV4.json?key=70ae23a3c11f473aa12a433d67bd4699&siteid=1231&timewindow=60'
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    mydata = (data['ResponseData']['Buses'])

    for d in mydata:
        if d['LineNumber'] in ['1','4','54'] and d['DisplayTime'] not in ['Nu','1 min','2 min']:
            busntime = d['LineNumber'] + ':an kommer om ' + d['DisplayTime']
            newdict = {'Busntime': busntime,  'Destination': d['Destination']}
            my_list.append(newdict)

    mydict.update(Buses = my_list)

    return jsonify(mydict)

@app.route('/schedule/')
def index():


    school = 'stims'
    username = 'elliot.linder'
    password = 'jordgubbe1'
    api = SchoolSoft(school, username, password)

    myschedule = api.fetch_schedule()
    return jsonify(myschedule)


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500
# [END app]
