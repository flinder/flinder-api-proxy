from bs4 import BeautifulSoup
import requests
import re
import json
import requests_toolbelt.adapters.appengine
####
from apiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
import pickle
####


# My personal keys for testing the API
try:
    import testkeys
except ImportError:
    pass # super dumb but i need it for debugging atm

class AuthFailure(Exception):
    """In case API authentication fails"""
    pass


class SchoolSoft(object):
    """SchoolSoft Core API (Unofficial)"""

    def __init__(self, school, username, password, usertype = 1):
        """
        school = School being accessed
        username = Username of account being logged in
        password = Password of account being logged in
        usertype = Type of account;
        0 = teacher, 1 = student
        """
        self.school = school

        self.username = username
        self.password = password
        self.usertype = usertype

        self.cookies = {}

        _login_page_re = r"https://sms(\d*).schoolsoft.se/%s/html/redirect_login.htm"
        self._login_page_re = re.compile(_login_page_re % school)

        # Might not be needed, still gonna leave it here
        self.login_page = "https://sms5.schoolsoft.se/{}/jsp/Login.jsp".format(school)

    def try_get(self, url, attempts = 0):
        """
        Tries to get URL info using
        self.username && self.password

        Mainly for internal calling;
        however can be used to fetch from pages not yet added to API.
        """
        r = requests.get(url, cookies=self.cookies)
        r.raise_for_status()
        login_page_match = self._login_page_re.match(r.url)
        if login_page_match:
            server_n = login_page_match.groups()
            if attempts < 1:
                # Sends a post request with self.username && self.password
                loginr = requests.post(self.login_page, data = {
                    "action": "login",
                    "usertype": self.usertype,
                    "ssusername": self.username,
                    "sspassword": self.password
                    }, cookies=self.cookies, allow_redirects=False)

                # Saves login cookie for faster access after first call
                self.cookies = loginr.cookies

                return self.try_get(url, attempts+1)
            else:
                raise AuthFailure("Invalid username or password")
        else:
            return r

    def fetch_lunch_menu(self):
        """
        Fetches the lunch menu for the entire week
        Returns an ordered list with days going from index 0-4
        This list contains all the food on that day
        """
        menu_html = self.try_get("https://sms5.schoolsoft.se/{}/jsp/student/right_student_lunchmenu.jsp?menu=lunchmenu".format(self.school))
        menu = BeautifulSoup(menu_html.text, "html.parser")

        lunch_menu = []

        for div in menu.find_all("td", {"style": "word-wrap: break-word"}):
            food_info = div.get_text(separator=u"<br/>").split(u"<br/>")
            lunch_menu.append(food_info)

        return lunch_menu


    def fetch_schedule(self):
        weekday=['Mon','Tue','Wed','Thu','Fri']
        caltype=['cal-news-type2','cal-news-type4','cal-lesson','cal-test']
        todaydict = {}
        mytests = []
        day = 0
        schedule_html = self.try_get("https://sms5.schoolsoft.se/{}/jsp/student/right_student_startpage.jsp".format(self.school))
        soup = BeautifulSoup(schedule_html.text, "html.parser")

        for i in range(5):
            date = 'date'+str(i)
            #print date
            divTag = soup.find_all("div", {"id": date})
            #print divTag
            for tag in divTag:
                for n in range(4):
                    tdTags = tag.find_all("div", class_=caltype[n])

                    for item in tdTags:
		        print weekday[i]
		        print item.text
		        print caltype[n]


        return todaydict

if __name__ == "__main__":
    # Testing shit, uses my testkeys
    api = SchoolSoft(testkeys.school, testkeys.username, testkeys.password)

    # Example calls
    lunch = api.fetch_lunch_menu()
    schedule = api.fetch_schedule()
